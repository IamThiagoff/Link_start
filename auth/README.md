## Auth
[Auth0](https://auth0.com/)

[JWT](https://jwt.io/)

[Clerk](https://clerk.com/)

[Zitadel](https://zitadel.com/)

[Stack](https://stack-auth.com/)

[Hanko](https://www.hanko.io/)

[SuperTokens](https://supertokens.com/)

[AuthJS](https://authjs.dev/)