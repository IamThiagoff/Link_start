<h1 align="center">
    📜 <br>
    Estudos de Tecnologia
</h1>

## 📚 Cursos de Tecnologia
- [Rocketseat](https://rocketseat.com.br/)
- [Udemy](https://www.udemy.com/pt/)
- [Coursera](https://www.coursera.org/)
- [edX](https://www.edx.org/)
- [Alura](https://www.alura.com.br/)
- [Pluralsight](https://www.pluralsight.com/)
- [Meu Curso | *Valor Simbólico*]()

## 📜 Cursos de Tecnologia com Certificado
- [Rocketseat](https://rocketseat.com.br/)
- [Udemy](https://www.udemy.com/pt/)
- [Coursera](https://www.coursera.org/)
- [edX](https://www.edx.org/)
- [Alura](https://www.alura.com.br/)
- [Khan Academy](https://www.khanacademy.org/)

## 💻 Organizações de Tecnologia 
- **JavaScript & Frameworks**
  - [React](https://pt-br.reactjs.org/)
  - [React Native](https://reactnative.dev)
  - [Vue](https://br.vuejs.org/)
  - [Angular](https://angular.io/)
  - [Node.js](https://nodejs.org/)
  - [Electron](https://www.electronjs.org/)
  - [TypeScript](https://www.typescriptlang.org/)
  - [Svelte](https://svelte.dev/)
  - [Next.js](https://nextjs.org/)
  - [Gatsby](https://www.gatsbyjs.com/)
  - [Nuxt.js](https://nuxtjs.org/)
  - [Ember.js](https://emberjs.com/)
  - [CoffeeScript](https://coffeescript.org)
  - [PrototypeJS](http://prototypejs.org/)
  - [MirageJS](https://miragejs.com)

- **Outras Tecnologias**
  - [PHP](https://www.php.net/manual/pt_BR/)
  - [PHP BR](http://br.phptherightway.com/)
  - [Python](https://python.org.br/)
  - [Django](https://www.djangoproject.com/)
  - [Flask](https://flask.palletsprojects.com/)
  - [GolangBR](http://www.golangbr.org/)
  - [Ruby](https://www.ruby-lang.org/pt/)
  - [Rails](https://rubyonrails.org/)
  - [Java](https://www.java.com/)
  - [Kotlin](https://kotlinlang.org/)
  - [Swift](https://swift.org/)
  - [Flutter](https://flutter.dev)
  - [Laravel](https://laravel.com)
  - [Symfony](https://symfony.com/)
  - [WordPress](https://br.wordpress.org/)
  - [Elixir](https://elixir-lang.org/)
  - [Rust](https://www.rust-lang.org/)

- **Estilos e UI/UX**
  - [CSS Tricks](https://css-tricks.com/)
  - [Sass](https://sass-lang.com)
  - [Less](https://lesscss.org/)
  - [Bootstrap](https://getbootstrap.com/)
  - [Foundation](https://get.foundation/)
  - [Materialize](https://materializecss.com/)
  - [Chakra UI](https://v2.chakra-ui.com/)
  - [Tailwind CSS](https://tailwindcss.com)
  - [Bulma](https://bulma.io/)
  - [Semantic UI](https://semantic-ui.com/)
  - [Gluestack](https://gluestack.io/)
  - [Mobbin](https://mobbin.com/browse)

- **Ferramentas e Configuração**
  - [ESLint](https://eslint.org/docs/latest/use/core-concepts/glossary)
  - [Prettier](https://prettier.io/)
  - [Babel](https://babeljs.io/)
  - [Webpack](https://webpack.js.org/)
  - [Parcel](https://parceljs.org/)
  - [Rollup](https://rollupjs.org/)
  - [TypeScript | tsconfig](https://github.com/microsoft/TypeScript/wiki/Node-Target-Mapping)

## 📝 Blogs e Artigos
- [Rocketseat Blog](https://blog.rocketseat.com.br/)
- [DEV.to](https://dev.to/)
- [FreeCodeCamp Blog](https://www.freecodecamp.org/news/)
- [CSS Tricks](https://css-tricks.com/)
- [Smashing Magazine](https://www.smashingmagazine.com/)
- [CodePen Blog](https://blog.codepen.io/)
- [Síndrome do Impostor: Superando Erros de Pensamento](https://www.tabnews.com.br/filipeleonelbatista/sindrome-do-impostor-superando-erros-de-pensamento)

## 📄 Medium
- [Como Fazer um README.md](https://medium.com/@raullesteves/github-como-fazer-um-readme-md-bonitão-c85c8f154f8)
- [Badges no GitHub](https://medium.com/@thiagoloureiro/badges-no-github-bf8289496c7d)
- [Guia Completo de JavaScript Moderno](https://medium.com/better-programming/the-complete-guide-to-modern-javascript-6e5e19bfcdd3)
- [O Guia Definitivo de Docker](https://medium.com/@thiagoloureiro/o-guia-definitivo-de-docker-bf8289496c7d)
- [Introdução ao DevOps](https://medium.com/@thiagoloureiro/introducao-ao-devops-bf8289496c7d)

## 🔗 Links Úteis
- [TypeHero](https://typehero.dev)
- [Mermaid](https://mermaid.js.org/)
- [LocalCan](https://localcan.com)
- [Serverless](https://www.serverless.com/)
- [WebHooks Cool](https://webhook.cool/at/strong-afternoon-17)
- [Figma](https://www.figma.com/)
- [Adobe XD](https://www.adobe.com/br/products/xd.html)
- [Sketch](https://www.sketch.com/)
- [InVision](https://www.invisionapp.com/)
- [React Native IDE](https://ide.swmansion.com/)
- [Expo](https://expo.dev/)
- [UploadThing](https://uploadthing.com/)
- [Algorithms Visualization](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html)
- [Big-O Cheat Sheet](https://www.bigocheatsheet.com/)
- [GitFluence](https://www.gitfluence.com/)
- [Cheatsheets](https://cheatsheets.zip/)
- [Rabbithook](http://www.rabbithook.com.br/)

## 📂 Repositórios e Documentação
- [System Design Primer](https://github.com/donnemartin/system-design-primer)
- [Free Programming Books](https://github.com/EbookFoundation/free-programming-books)
- [Public APIs](https://github.com/public-apis/public-apis)
- [Awesome](https://github.com/sindresorhus/awesome)
- [Scalar | API Docs](https://docs.scalar.com/project-default/version-default/guide-default/page-default)

## 📖 Criação de Currículo
- [HackerRank Resume](https://www.hackerrank.com/resume/dashboard)
- [Canva](https://www.canva.com/)
- [NovoResume](https://novoresume.com/)
- [Resume.io](https://resume.io/)

## 💼 Praticar para Entrevistas
- [LeetCode](https://leetcode.com/)
- [CodeSignal](https://codesignal.com/)
- [StrataScratch](https://www.stratascratch.com/)
- [Great FrontEnd](https://www.greatfrontend.com/pt-BR)
- [HackerRank](https://www.hackerrank.com/)
- [Exercism](https://exercism.io/)
- [Interview Cake](https://www.interviewcake.com/)
- [ResumeGo](https://www.resumego.net/)

## 🤖 Inteligência Artificial
- [Typebot](https://app.typebot.io)
- [OpenAI](https://openai.com/)
- [Hugging Face](https://huggingface.co/)
- [IBM Watson](https://www.ibm.com/watson)
- [Google AI](https://ai.google/)

## 🎨 Estilo e Design
- [Google Fonts](https://fonts.google.com/)
- [Font Awesome](https://fontawesome.com/)
- [Material Icons](https://material.io/resources/icons/)
- [Heroicons](https://heroicons.com/)
- [Lucide Icons](https://lucide.dev/)

## 🗂️ Ferramentas All-in-One
- [SmallPDF](https://smallpdf.com/pt#r=app)
- [PDFEscape](https://www.pdfescape.com/)
- [ILovePDF](https://www.ilovepdf.com/)
- [Canva](https://www.canva.com/)

## 📧 Emails e Comunicação
- [React Mail](https://react.email/docs/introduction)
- [Mailchimp](https://mailchimp.com/)
- [SendGrid](https://sendgrid.com/)
- [

Next Intl](https://next-intl-docs.vercel.app/)
- [Mailjet](https://www.mailjet.com/)

## 🎵 Edição de Áudio
- [Bandlab](https://www.bandlab.com/feed/trending)
- [Moises](https://studio.moises.ai/library/)
- [Audacity](https://www.audacityteam.org/)
- [Soundtrap](https://www.soundtrap.com/)

## ⚙️ Sistemas Operacionais
- [Rufus](https://rufus.ie/pt_BR/)
- [VirtualBox](https://www.virtualbox.org/)
- [VMware](https://www.vmware.com/)
- [Linux Mint](https://linuxmint.com/)
- [Ubuntu](https://ubuntu.com/)

## 🖥️ IDEs Online
- [VSCode Web](https://vscode.dev/)
- [Replit](https://replit.com/)
- [Gitpod](https://gitpod.io/)
- [CodeSandBox](https://codesandbox.io/)
- [StackBlitz](https://stackblitz.com/)
- [Glitch](https://glitch.com/)
- [JSFiddle](https://jsfiddle.net/)
- [Querybook](https://www.querybook.org/)

## 🛠️ Ferramentas para Criar README
- [Readme Generator](https://profile-readme-generator.com/)
- [ReadMe.so](https://readme.so/)
- [Make a README](https://www.makeareadme.com/)

## 📷 Imagens
- [Freepik](https://www.freepik.com/)
- [Unsplash](https://unsplash.com/)
- [Pexels](https://www.pexels.com/)
- [Pixabay](https://pixabay.com/)
- [IconFinder](https://www.iconfinder.com/)

## 📝 Aplicativos de Produtividade
- [Notion](https://www.notion.so/)
- [Xmind](https://www.xmind.net/)
- [Forest](https://www.forestapp.cc/)
- [Trello](https://trello.com/)
- [Focus To-Do](https://www.focustodo.cn/)
- [Evernote](https://evernote.com/)
- [Todoist](https://todoist.com/)
- [Microsoft To Do](https://todo.microsoft.com/)

## 🌐 Aprender Inglês
- [Test English](https://test-english.com/)
- [Livekit](https://kitt.livekit.io/)
- [Busuu](https://www.busuu.com/)
- [Duolingo](https://www.duolingo.com)
- [BBC Learning English](https://www.bbc.co.uk/learningenglish)
- [Cambridge English](https://www.cambridgeenglish.org/)
- [Lingoda](https://www.lingoda.com/)